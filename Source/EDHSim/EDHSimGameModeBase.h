// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EDHSimGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EDHSIM_API AEDHSimGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
