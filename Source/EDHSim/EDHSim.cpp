// Copyright Epic Games, Inc. All Rights Reserved.

#include "EDHSim.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EDHSim, "EDHSim" );
