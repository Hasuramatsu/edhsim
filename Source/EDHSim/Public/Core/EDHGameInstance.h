// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "EDHGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class EDHSIM_API UEDHGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
